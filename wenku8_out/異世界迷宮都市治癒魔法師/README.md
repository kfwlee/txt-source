# novel

- title: 異世界の迷宮都市で治癒魔法使いやってます
- title_zh1: 來到異世界迷宮都市的我成了治癒魔法師
- title_zh: 異世界迷宮都市治癒魔法師
- author: 幼馴じみ
- illust:
- source: http://ncode.syosetu.com/n2740ca/
- cover: http://img.wenku8.com/image/1/1938/1938s.jpg
- publisher: syosetu
- date: 2018-12-28T23:13:00+08:00
- status: 連載
- novel_status: 0x0300

## illusts


## publishers

- wenku8

## series

- name: 異世界の迷宮都市で治癒魔法使いやってます

## preface


```
大学生佐藤四季清醒时，发现自己已经身处异世界迷宫都市。
在这个世界拥有治愈魔法的他，因为总是用性骚扰当成疗伤的代价，终于被原本工作的治疗院开除。
走投无路的四季为了活下去，决定成为冒险者，潜入迷宫。
之后，他遇到受伤的奴隶少女悠艾儿……

現代日本の大学生である佐藤四季（サトウシキ）はある日突然通り魔に襲われ、目が覚めると異世界の迷宮都市に飛ばされていた。そこで治癒魔法に目覚めた彼は、怪我を治癒魔法で治す代わりにセクハラしたり、奴隷を買って迷宮に潜ってみたり、冒険者仲間にセクハラしたり。そんな感じの作品になる予定です。

モンスター文庫様から書籍化しました。
小説全五巻発売中です。
モンスターコミックス様にてコミカライズも連載中です。
本編完結しました。
```

## tags

- node-novel
- wenku8
- node-novel
- R15
- syosetu
- ダークエルフ
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ハーレム
- ファンタジー
- 奴隷
- 残酷な描写あり
- 治癒魔法チート
- 異世界
- 異世界転移
- 迷宮

# contribute

- 轻之国度
- kerorokun
- a8901566
- 零时
- 化物语
- 音无
- 风
- zbszsr
- bulbfrm
- 农夫绅士
- 撸管娘

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 4
- startIndex: 1

## textlayout

- allow_lf2: true

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n2740ca

## wenku8

- novel_id: 1938

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n2740ca&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n2740ca/)
- 
