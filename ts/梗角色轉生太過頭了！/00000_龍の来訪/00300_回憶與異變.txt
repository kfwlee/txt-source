『──我們魔族和人能成為朋友。但是啊，無法和人類共存！』

有著比誰都更充滿了慈祥的眼睛的以前的魔王。
對莉諾亞來說給人嬌慣孫女的溫柔祖父印象的某個青年，不知什麼時候變得會像口頭禪一樣那樣講述。
儘管會露出可怕的表情，莉諾亞依然能在腦海中最先回想起他溫柔的笑容。

那是距今兩千年以上的過去的事。

約爾諾森林還是個小森林，是某個貴族所有的領地的時代，魔族面對某一個人的存在即將統一。
時而對同族，時而對外敵，時而對人類，花費漫長的歳月經歷了各種各樣的戰鬥的魔物進化成為魔族的人全都對自己的力量擁有著驕傲與自信。
因此，在誰的底下統一絶不可能。
因為各自只捉將同族作為自己的糧食的獵物。
當然，甚至不認識貴族等架空的地位。有的只是被極大的力量支持的弱肉強食的秩序。

可是，即便是那樣的魔族也覺得挑起戰鬥是愚蠢的，

壓倒性的，

絶對的，

力量的持有者出生了。

據說既是初代也是唯一的魔王，倫基・西諾哈拉是有著一根神聖的角和如烈火般火紅的雙眸的少年。【譯註：這個名字為什麼這麼怪在下文會講】

雖然對莉諾亞來說是祖父大人，但他的外表只是有著水靈的肌膚的青年。

被他擁有的力量所吸引，高位魔族構築秩序，慢慢地靠本能生活的魔物們、魔族作為國家統一了。
可是，過於強大的力量集結給了許多種族危機感。
特別是，給事實上支配著大陸的人類們帶去的影響不可估量。
據說即使僅有一個魔族也會變成一個都市或一個國家應對的事態的魔族團結一致建國了。
各國對那個趨勢焦躁起來。
國家間的戰爭瞬間停止，甚至多種族築起了協調關係是必然的，真是莫大的諷刺。

但是，儘管如此魔族維持了中立的外交，沒有支持某處的一國並發動戰爭，沒有圍繞霸權爭鬥。只是修築能普通地、平穩地、和平地生活的場所。不難想像那有多麼難，但莉諾亞知道的倫基就是那樣說的人物。

人類維持在各國的協力關係，圖謀對魔族的均衡。
那也是那種危險的均衡勉勉強強被維持的時代。
但是，那種像針尖上的板一樣的安定因為某個存在的出現一口氣崩潰了。

選神教這一宗教瞬間在大陸全境蔓延了。
其信仰被神選中的存在是人類，證據是我等有確實的理智。
魔族是魔物的延長線，那個醜陋又吃人的沒有理性的姿態正是邪惡的本性，選神教如此講述。
他們的語言是從人那奪來的，力量是從神和人類那奪取的。

莉諾亞覺得真虧他們騙得出來。
魔物們像日常一樣重複賭上自己性命的鬥爭，在此之上得到的力量。所以擁有著驕傲。選神教的教義要讓魔族激怒足夠過頭了。

人類至上主義。

如果說是沒有根據的妄想的話也就是這樣了，但把不安強加給魔族，優遇人的他們的教義輕易地在人類間傳播了。
許多國家接受了那種宗教，將弊政、貧困和剝削的理由全部推卸給了方便的存在。

於是，魔族和人類變成了互不相容的存在。
在那樣的宗教擁有力量的背景下有勇者這樣一個存在。
選神教半強制地將不知什麼時候出現的青年認定為了勇者。

那絶不是有約束力的東西，只是作為民眾的希望──捧了表舞台的英雄。不知不覺人們變得會自然地說出勇者這個詞了。
那極大的力量有時被派遣向襲擊村子的魔物，有時為了拯救村莊女孩被派遣，有時為了守護國家被派遣。那樣的小小善行也產生了不得不介入與魔族的爭端的場面。
小小的爭鬥產生憎恨的連鎖，束縛了勇者的力量。
不知不覺，以勇者和魔王為中心戰火擴大了，許多人被不幸所左右。

對莉諾亞來說，人類是祖父和父母的仇人，是讓對魔族來說是驕傲的力量應有的狀態歪曲了的卑鄙無恥的人。
因為那種存在遭受了敗北也好，被奪走了溫柔的祖父也好，失去了雙親也好，被奪走了領地也好，自己的力量不足也好，全部升華為了憎恨。
那樣的憎恨給了復仇理由。
現在一定要創造出曾經溫柔的祖父理想的，魔族能和平生活的場所，將這個變成了復仇的理由。


「勇者大人有個最愛的人。我想正因為在小教會工作的那個少女──那個少女想要保護的東西對他來說是最重要的，所以他才和祖父大人戰鬥」

邊哭邊和櫻相抱，不久冷靜下來的莉諾亞繼續說起過去的事。時而高興，時而寂寞，時而有怨氣，娜哈特只是一直聽著講述過去的莉諾亞的話。

不知什麼時候，她的語調不是裝門面的傲慢，也不是像與年齡相稱的孩子，而是變成了像好好受過教育的貴族一樣。
對印象差別過大，變得陰郁的少女，娜哈特感到了違和感。

「勇者大人？」

為何要對敵人，而且是最可恨的仇敵加上大人呢。娜哈特詢問莉諾亞。

「勇者大人和魔王大人是戰友。雖然很多時候互相戰鬥，但好像也會對酌。我覺得他們互相碰撞，或者說必須互相碰撞的最主要的理由是人的利己主義。當然，魔物的統治不充分我認為是作為貴族的我們有漏洞」

娜哈特儘管聽著莉諾亞的說明，卻無法忍受違和感了。
如同說服了一般，娜哈特對莉諾亞說。

「能別用那個語調了嗎？有點發寒⋯⋯」
「什麼啊！真失禮呢！明明是因為敗給你了才這樣，全恭敬地說話的！」

就算被這樣說，傲慢的少女突然老成也是會抓狂的。
果然還是這邊的語調比較對勁。

「後來，在那樣的戰爭中侍奉祖父大人的大貴族的我的爸爸和媽媽忙得很難陪我。擔心那樣的我的祖父大人在我十五歳生日那天給了我櫻。我的裝備是二十歳左右的時候得到的吧？因為是很高價的東西所以我謝絶了──但我記得祖父大人說比起作為倉庫的肥料還是用起來比較好之類的，給了我各種各樣的東西哦。真的是，各種各樣的」

回想起過去，有些欣慰地放鬆了臉頰的莉諾亞說道。

「是嗎──」

那個魔王筱原・蓮司肯定是日本人，和娜哈特一樣是Real World Online的玩家吧。可是，即使說了現實（Real）的名字，娜哈特也無法推測那是哪裡的哪個人。

但是娜哈特想至少不是公會成員吧。
異世界咖啡館（Outer Cafeteria）的成員裡只存在兩個魔族。一個是徹的主角色，另一個是怠惰的效率主義者的男人。雖然這樣說不太好，但不得不認為他不是會希望和平的人。

「雖說魔王敗給了勇者，但勇者還存在著嗎？」

如果戰勝了魔王的勇者存在的話，對娜哈特來說或許會成為威脅。
那樣覺得便問了莉諾亞，但她一邊失笑一邊開口道。

「怎麼可能。人類不可能能活幾千年吧。我沉睡之後經過了兩千年以上所以早就衰老了啦。他對祖父大人提出要一對一戰鬥也是因為，如果他死了的話人類肯定會敵不過我們了吧。倒不如說有像你一樣的存在出乎我的意料啦！犯規的吧，你是⋯⋯」

想起來從最初莉諾亞就給人孩子一樣的印象。
莉諾亞一定甚至都沒有設想過敗北吧。所以忘記了戰鬥的覺悟，就那樣任憑憎恨去復仇了。

另一方面確實也出乎娜哈特的意料。
娜哈特再次認識到這個世界曾經也存在和娜哈特相同的人。
也能知道那個人留下的裝備和櫻現在也就這樣存在著。
娜哈特覺得得到了就個人來說很滿意的成果。
然後，忽然看了失去了一隻手的櫻。

「啊啊，說起來，做了抱歉的事吶──來──」

娜哈特從倉庫取出兩個上級回復藥（High Potion），撒在了莉諾亞的手和櫻的手臂上。
於是光的粒子集中，莉諾亞的傷和櫻的手臂被光包住了。
沒有花一秒的時間吧。
兩人的傷像沒有存在過一樣消失了。

「⋯⋯⋯⋯真是毫無道理呢⋯⋯簡直就像，祖父大人一樣⋯⋯」

連部位缺損的櫻的手臂都在沐浴光後恢復了原樣。
當然從這個世界的常識考慮的話那是不可能的事，但在遊戲時代的常識裡可以說是當然的。

「魔道人偶，記得正式是叫魔道制御人偶（Automata）嗎？嘛，因為回復當然也對NPC有效，所以會受到回復藥（Potion）的影響吧」

雖說失去了一隻手，但HP確實還有一半以上吧。

作為原初的深暗（Darkness）的特殊能力的對接近對象的持續傷害的效果現在升華為了自動靈活地攻擊敵人的迎擊能力。但是，無論古代級（Ancient）的裝備有多麼優秀，只是持續傷害在對象是五十級的對手的場合是否會在幾分鐘裡削減兩成呢。

一瞬間就撕碎了手臂，顯而易見和娜哈特的技能（Skill）一樣被大幅強化了。雖然是作為強力效果的代價弱點也有很多的裝備，但娜哈特對這個極端特化攻擊的裝備涌現了愛戀。會覺得那樣的愛戀響應了不管怎麼說也是太愛做夢了，娜哈特這樣想著搖了搖頭。

「非常感謝」

那樣說著櫻低下了頭。
那是對治療了自己手臂的娜哈特呢，還是對放過了主人性命的娜哈特呢。
至少，櫻的眼睛裡傾注了明確的意思。
娜哈特無法將那個斷言為NPC。
硬要說的話，接近有心的人。
愉快地體味著那種變化時，莉諾亞直直地看了娜哈特。

「是我輸了⋯⋯隨你便了⋯⋯」
「嗯？可是，我的目的已經達成了。我對你沒有更多要求？」

娜哈特像理所當然一樣地說道，但莉諾亞好像想說莫名其妙似的表情扭曲了。

「哈啊？可以嗎？放過我？我說不定還會對人類們做什麼哦？
祖父大人經常說──無法和人類共存。
我也那樣認為。要和什麼都怪我們，從陷害到殲滅戰都不在乎地正當化的那些垃圾們搞好關係絶對做不到！
不在這裡想辦法解決我的話，以後也許會變成大麻煩哦？」

雖然莉諾亞那樣說，但娜哈特另有擔憂。
她在戰鬥中不是避開也不是防御娜哈特的魔法────而是斬斷了。

那是遊戲時代娜哈特的主角色擅長的技藝。
正因為如此，將來這個少女成長了時，有成為娜哈特的天敵的可能性。
娜哈特的擔憂就是這一點，關於她這之後要怎麼辦則不太有興趣。

「你要恨人類的話我不會阻止你。要發動戰爭的話也可以吧。這次只是碰巧我一時興起參戰了。我並沒有打算支持人。戰爭是世間常態。被憎恨所左右有時也會有。無法割捨的思念也確實會有。但是啊，起事的時候最好慢慢地回憶自己的珍愛」

對始終高高在上的娜哈特的聲音，莉諾亞微微笑了。

「是嗎⋯⋯⋯⋯祖父大人的話的意思，感覺有點懂了──」

少女剛一那樣說，北方的天空就發生了異變。
像要奪走天空的光一樣的巨軀從距離了數十千米的這個森林也能映入眼簾。那是甚至感到懷念的氣氛的表現。
娜哈特的知覺比誰都先察知了那個。

「──喂──你沒說那個是你的秘密兵器啊？」

對娜哈特的話莉諾亞呆然了，像忽然回過神來一樣搖頭了。

「⋯⋯怎麼會，就算再怎麼說我是魔族，那種也⋯⋯不可能能支配⋯⋯⋯⋯」
「抱歉，現在好像不是悠閑地說話的時候」

娜哈特從自己的手腕上取下了咒具。

「什────！」

看到威壓感增加了的娜哈特，莉諾亞發出了奇聲怪叫。

但是，娜哈特對此沒有回答。
娜哈特胸中抱有小小的不安，已經是跑出去後了。
連空氣的破碎聲都在背後，娜哈特一溜煙地跑出去了。